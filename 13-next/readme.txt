NEXT JS

- Static render: Build cервер дээр HTML файл үүсгэнэ
- Built-in routes: Файлаар замаа тохируулдаг
- Pre-fetching: Урьдчилаад замуудаа дуудна
- Server side render: Сервер дээр зурна
- Image optimization: Яг тохирсон хэмжээтэй, нүдэнд харагдах үед дуудна
- API routes: Бакэнд замууд
- Zero config: Тохируулгагүйгээр сервэр дээр байршуулах боломжтой
- CSS styled-jsx
...

