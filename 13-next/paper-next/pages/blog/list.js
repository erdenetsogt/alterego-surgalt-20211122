import Image from "next/image";
import Head from "next/head";

export default function Blog() {
    return (
        <div>
            <Head>
                <title>Blog list</title>
            </Head>
            <div style={{ height: 2000 }}>Blog list</div>
            Lazy loading (Eager loading)
            <Image priority={true} quality={100} src="https://content.ikon.mn/news/2021/12/20/mj9y9g___x974.jpg" alt="Ikon Image" width={1100} height={1100} />
        </div>
    );
}
