export default function Blog() {
    return (
        <>
            <div className="root">Blog root</div>
            <style jsx>{`
                .root {
                    color: red;
                    font-size: 30px;
                }
                .root:hover {
                    color: green;
                    font-size: 30px;
                }
            `}</style>
        </>
    );
}
