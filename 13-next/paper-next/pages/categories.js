import { useEffect, useState } from "react";

export default function Categories(props) {
    const [list, setList] = useState([]);

    useEffect(() => {
        fetch("api/categories")
            .then((response) => response.json())
            .then((data) => setList(data));
    }, []);

    return (
        <div style={{ width: 200 }}>
            {list.map((item) => (
                <div key={item.name} style={{ textAlign: "center", border: "1px solid #000", padding: "1rem", marginBottom: "1rem" }}>
                    {item.name}
                </div>
            ))}
        </div>
    );
}

// // хүсэлт илгээх болгонд дуудагдана
// export async function getServerSideProps(context) {
//     console.log("----getServerSideProps----");

//     const list = await fetch("http://localhost:3000/api/categories").then((response) => response.json());

//     return {
//         props: { list },
//     };
// }

//Build хийх үед нэг л удаа дуудагдана
export async function getStaticProps(context) {
    console.log("----getStaticProps----");

    const list = await fetch("https://paper-next.vercel.app/api/categories").then((response) => response.json());

    return {
        props: { list },
    };
}
