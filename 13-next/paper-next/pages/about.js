import Link from "next/link";

export default function About() {
    return (
        <div>
            About page{" "}
            <Link href="/blog/list">
                <a>blog list</a>
            </Link>
        </div>
    );
}
