import "../styles/globals.css";

function MyApp({ Component, pageProps }) {
    return (
        <div style={{ background: "whitesmoke" }}>
            <Component {...pageProps} />
        </div>
    );
}

export default MyApp;
