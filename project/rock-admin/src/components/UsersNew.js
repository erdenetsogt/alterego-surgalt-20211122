import React, { useState } from "react";
import Fab from "@mui/material/Fab";
import Box from "@mui/material/Box";
import AddIcon from "@mui/icons-material/Add";
import { Button, DialogTitle, Dialog, TextField, DialogContent, DialogActions } from "@mui/material";
import { toast } from "react-toastify";
import adminFetcher from "../utils/adminFetcher";

export default function UsersNew({ onChange }) {
    const [open, setOpen] = useState(false);
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [loading, setLoading] = useState(false);

    async function submit() {
        setLoading(true);

        const form = { username, password };

        const newUser = await adminFetcher(`users`, "POST", form);

        if (newUser) {
            setLoading(false);
            setOpen(false);
            onChange();
            setUsername("");
            setPassword("");
            toast("Амжилттай хадгаллаа");
        }
    }

    return (
        <>
            <Fab color="secondary" aria-label="add" onClick={() => setOpen(true)}>
                <AddIcon />
            </Fab>

            <Dialog onClose={() => !loading && setOpen(false)} open={open}>
                <DialogTitle>Шинэ хэрэглэгч</DialogTitle>
                <DialogContent>
                    <Box sx={{ pt: 2 }}>
                        <TextField disabled={loading} label="Хэрэглэгчийн нэр" variant="outlined" value={username} onChange={(e) => setUsername(e.target.value)} />
                    </Box>
                    <Box sx={{ pt: 2 }}>
                        <TextField type="password" disabled={loading} label="Нууц үг" variant="outlined" value={password} onChange={(e) => setPassword(e.target.value)} />
                    </Box>
                </DialogContent>
                <DialogActions>
                    <Button disabled={loading} onClick={() => setOpen(false)}>
                        Болих
                    </Button>
                    <Button disabled={loading} variant="contained" onClick={submit}>
                        Хадгалах
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
}
