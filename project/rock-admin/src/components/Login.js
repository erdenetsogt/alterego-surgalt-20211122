import React, { useState } from "react";
import Box from "@mui/material/Box";
import { Button, TextField, Container } from "@mui/material";
import adminFetcher from "../utils/adminFetcher";

export default function Login() {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [loading, setLoading] = useState(false);

    async function submit() {
        setLoading(true);

        const form = { username, password };

        const data = await adminFetcher(`login`, "POST", form);

        if (!data) {
            setLoading(false);
            alert("Нууц үг эсвэл хэрэглэгчийн нэр буруу байна!");
        }

        localStorage.setItem("token", data[0]);
        alert("Амжилттай нэвтэрлээ.");
        window.location.reload();
    }

    return (
        <Container maxWidth="xs" sx={{ mt: 2 }}>
            <Box sx={{ pt: 2 }}>
                <TextField fullWidth disabled={loading} label="Хэрэглэгчийн нэр" variant="outlined" value={username} onChange={(e) => setUsername(e.target.value)} />
            </Box>
            <Box sx={{ pt: 2 }}>
                <TextField fullWidth type="password" disabled={loading} label="Нууц үг" variant="outlined" value={password} onChange={(e) => setPassword(e.target.value)} />
            </Box>

            <Button fullWidth disabled={loading} variant="contained" onClick={submit} sx={{ mt: 3 }}>
                Нэвтрэх
            </Button>
        </Container>
    );
}
