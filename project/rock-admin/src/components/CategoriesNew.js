import React, { useState } from "react";
import Fab from "@mui/material/Fab";
import Box from "@mui/material/Box";
import AddIcon from "@mui/icons-material/Add";
import { Button, DialogTitle, Dialog, TextField, DialogContent, DialogActions, Select, MenuItem, InputLabel, FormControl } from "@mui/material";
import { toast } from "react-toastify";
import adminFetcher from "../utils/adminFetcher";
import { useSWRConfig } from "swr";

const colors = [
    { value: "is-info", label: "Цэнхэр" },
    { value: "is-success", label: "Ногоон" },
    { value: "is-warning", label: "Шар" },
    { value: "is-link", label: "Хөх" },
    { value: "is-primary", label: "Номин" },
    { value: "is-danger", label: "Улаан" },
];

export default function CategoriesNew() {
    const [open, setOpen] = useState(false);
    const [name, setName] = useState("");
    const [color, setColor] = useState("");
    const [loading, setLoading] = useState(false);

    const { mutate } = useSWRConfig();

    async function submit() {
        setLoading(true);

        const form = { name, color };

        const newCategory = await adminFetcher(`categories`, "POST", form);

        if (newCategory) {
            setLoading(false);
            setOpen(false);
            mutate("categories");
            setName("");
            toast("Амжилттай хадгаллаа");
        }
    }

    return (
        <>
            <Fab color="secondary" aria-label="add" onClick={() => setOpen(true)}>
                <AddIcon />
            </Fab>

            <Dialog onClose={() => !loading && setOpen(false)} open={open}>
                <DialogTitle>Шинэ ангилал</DialogTitle>
                <DialogContent>
                    <Box sx={{ pt: 2 }}>
                        <TextField disabled={loading} label="Нэр" variant="outlined" value={name} onChange={(e) => setName(e.target.value)} />
                    </Box>
                    <Box sx={{ pt: 2 }}>
                        <FormControl fullWidth>
                            <InputLabel id="demo-simple-select-label">Өнгө</InputLabel>
                            <Select labelId="demo-simple-select-label" id="demo-simple-select" value={color} label="Өнгө" onChange={(e) => setColor(e.target.value)}>
                                {colors.map((item) => (
                                    <MenuItem key={item.value} value={item.value}>
                                        {item.label}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Box>
                </DialogContent>
                <DialogActions>
                    <Button disabled={loading} onClick={() => setOpen(false)}>
                        Болих
                    </Button>
                    <Button disabled={loading} variant="contained" onClick={submit}>
                        Хадгалах
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
}
