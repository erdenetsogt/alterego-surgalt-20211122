import React from "react";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import ArticlesList from "./ArticlesList";

export default function Categories() {
    return (
        <Container maxWidth="sm" sx={{ mt: 2 }}>
            <Box sx={{ display: "flex", alignItems: "center", justifyContent: "space-between", mb: 2 }}>
                <Typography variant="h2">Нийтлэл</Typography>
            </Box>

            <ArticlesList />
        </Container>
    );
}
