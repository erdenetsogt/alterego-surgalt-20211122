import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import { Button, DialogTitle, Dialog, TextField, DialogContent, DialogActions, Select, MenuItem, InputLabel, FormControl } from "@mui/material";
import { toast } from "react-toastify";
import adminFetcher from "../utils/adminFetcher";
import { useSWRConfig } from "swr";

const colors = [
    { value: "is-info", label: "Цэнхэр" },
    { value: "is-success", label: "Ногоон" },
    { value: "is-warning", label: "Шар" },
    { value: "is-link", label: "Хөх" },
    { value: "is-primary", label: "Номин" },
    { value: "is-danger", label: "Улаан" },
];

export default function CategoriesEdit({ editing, onClose }) {
    const [name, setName] = useState("");
    const [color, setColor] = useState("");
    const [loading, setLoading] = useState(false);

    const { mutate } = useSWRConfig();

    useEffect(() => {
        if (editing) {
            adminFetcher(`categories/${editing}`).then((data) => {
                setName(data.name);
                setColor(data.color);
            });
        }
    }, [editing]);

    async function submit() {
        setLoading(true);

        const form = { name, color };

        await adminFetcher(`categories/${editing}`, "PUT", form);

        setLoading(false);
        onClose();
        mutate("categories");
        setName("");
        setColor("");
        toast("Амжилттай заслаа");
    }

    return (
        <Dialog onClose={() => !loading && onClose()} open={!!editing}>
            <DialogTitle>Засах</DialogTitle>
            <DialogContent>
                <Box sx={{ pt: 2 }}>
                    <TextField disabled={loading} label="Нэр" variant="outlined" value={name} onChange={(e) => setName(e.target.value)} />
                </Box>
                <Box sx={{ pt: 2 }}>
                    <FormControl fullWidth>
                        <InputLabel id="demo-simple-select-label">Өнгө</InputLabel>
                        <Select labelId="demo-simple-select-label" id="demo-simple-select" value={color} label="Өнгө" onChange={(e) => setColor(e.target.value)}>
                            {colors.map((item) => (
                                <MenuItem key={item.value} value={item.value}>
                                    {item.label}
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </Box>
            </DialogContent>
            <DialogActions>
                <Button disabled={loading} onClick={onClose}>
                    Болих
                </Button>
                <Button disabled={loading} variant="contained" onClick={submit}>
                    Хадгалах
                </Button>
            </DialogActions>
        </Dialog>
    );
}
