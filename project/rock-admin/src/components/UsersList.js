import React, { useEffect, useState } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { Button, Typography } from "@mui/material";
import ConfirmDelete from "./ConfirmDelete";
import UsersEdit from "./UsersEdit";
import { toast } from "react-toastify";
import CircularProgress from "@mui/material/CircularProgress";
import Box from "@mui/material/Box";
import adminFetcher from "../utils/adminFetcher";

export default function UsersList({ changedAt }) {
    const [list, setList] = useState(null);

    const [deleting, setDeleting] = useState("");
    const [editing, setEditing] = useState("");

    const [loading, setLoading] = useState(false);

    async function loadList() {
        const data = await adminFetcher("users");
        setList(data);
    }

    useEffect(() => {
        loadList();
    }, [changedAt]);

    async function handleDelete() {
        setLoading(true);
        await adminFetcher(`users/${deleting}`, "DELETE");

        setLoading(false);
        setDeleting("");
        loadList();
        toast("Амжилттай устгалаа");
    }

    if (list === null) {
        return (
            <Box sx={{ textAlign: "center" }}>
                <CircularProgress />
            </Box>
        );
    }

    if (list.length === 0) {
        return (
            <Box sx={{ textAlign: "center" }}>
                <Typography variant="h1" sx={{ opacity: 0.3 }}>
                    Хоосон
                </Typography>
            </Box>
        );
    }

    return (
        <>
            <TableContainer component={Paper}>
                <Table aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell></TableCell>
                            <TableCell>Нэр</TableCell>
                            <TableCell></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {list.map((row, index) => (
                            <TableRow key={row._id}>
                                <TableCell>{index + 1}</TableCell>
                                <TableCell>{row.username}</TableCell>
                                <TableCell align="right">
                                    <Button color="error" onClick={() => setDeleting(row._id)}>
                                        Устгах
                                    </Button>
                                    <Button onClick={() => setEditing(row._id)}>Засах</Button>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>

            <ConfirmDelete loading={loading} open={deleting} onClose={() => setDeleting("")} onCorfirm={handleDelete} />
            <UsersEdit editing={editing} onClose={() => setEditing("")} onChange={loadList} />
        </>
    );
}
