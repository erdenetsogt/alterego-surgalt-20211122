import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import { Button, DialogTitle, Dialog, TextField, DialogContent, DialogActions } from "@mui/material";
import { toast } from "react-toastify";
import adminFetcher from "../utils/adminFetcher";

export default function UsersEdit({ onChange, editing, onClose }) {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if (editing) {
            adminFetcher(`users/${editing}`).then((data) => {
                setUsername(data.username);
            });
        }
    }, [editing]);

    async function submit() {
        setLoading(true);

        const form = { username, password };

        await adminFetcher(`users/${editing}`, "PUT", form);

        setLoading(false);
        onClose();
        onChange();
        setUsername("");
        setPassword("");
        toast("Амжилттай заслаа");
    }

    return (
        <Dialog onClose={() => !loading && onClose()} open={!!editing}>
            <DialogTitle>Засах</DialogTitle>
            <DialogContent>
                <Box sx={{ pt: 2 }}>
                    <TextField disabled={loading} label="Хэрэглэгчийн нэр" variant="outlined" value={username} onChange={(e) => setUsername(e.target.value)} />
                </Box>
                <Box sx={{ pt: 2 }}>
                    <TextField type="password" disabled={loading} label="Нууц үг" variant="outlined" value={password} onChange={(e) => setPassword(e.target.value)} />
                </Box>
            </DialogContent>
            <DialogActions>
                <Button disabled={loading} onClick={onClose}>
                    Болих
                </Button>
                <Button disabled={loading} variant="contained" onClick={submit}>
                    Хадгалах
                </Button>
            </DialogActions>
        </Dialog>
    );
}
