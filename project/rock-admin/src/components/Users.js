import React, { useState } from "react";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import UsersNew from "./UsersNew";
import UsersList from "./UsersList";

export default function Users() {
    const [changedAt, setChangedAt] = useState(null);

    function handleChange() {
        setChangedAt(Date.now());
    }

    return (
        <Container maxWidth="sm" sx={{ mt: 2 }}>
            <Box sx={{ display: "flex", alignItems: "center", justifyContent: "space-between", mb: 2 }}>
                <Typography variant="h2">Хэрэглэгч</Typography>
                <UsersNew onChange={handleChange} />
            </Box>

            <UsersList changedAt={changedAt} />
        </Container>
    );
}
