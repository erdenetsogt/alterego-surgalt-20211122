import React, { useState } from "react";
import Box from "@mui/material/Box";
import { Button, TextField, Container, Typography, Select, MenuItem, InputLabel, FormControl } from "@mui/material";
import { toast } from "react-toastify";
import { gql, useMutation } from "@apollo/client";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import adminFetcher from "../utils/adminFetcher";
import { useNavigate } from "react-router-dom";
import useSWR from "swr";

const ADD_ARTICLE = gql`
    mutation AddArticle($title: String!, $slug: String, $categoryId: String, $text: String) {
        addArticle(title: $title, slug: $slug, categoryId: $categoryId, text: $text) {
            _id
            title
        }
    }
`;

export default function CategoriesNew() {
    const navigate = useNavigate();

    const [addArticle, { loading }] = useMutation(ADD_ARTICLE, {
        onCompleted({ addArticle }) {
            if (addArticle) {
                setTitle("");
                setSlug("");
                toast("Амжилттай хадгаллаа");
                navigate("/articles");
            } else {
                toast.error("Slug давхардсан байна");
            }
        },
    });

    const [title, setTitle] = useState("");
    const [slug, setSlug] = useState("");
    const [text, setText] = useState("");
    const [categoryId, setCategoryId] = useState("");

    async function submit() {
        addArticle({
            variables: { title, slug, categoryId, text },
        });
    }

    return (
        <>
            <Container maxWidth="sm" sx={{ mt: 2 }}>
                <Box sx={{ display: "flex", alignItems: "center", justifyContent: "space-between", mb: 2 }}>
                    <Typography variant="h2">Шинэ нийтлэл</Typography>
                </Box>

                <CategorySelector value={categoryId} onChange={setCategoryId} />

                <Box sx={{ mb: 2 }}>
                    <TextField fullWidth disabled={loading} label="Гарчиг" variant="outlined" value={title} onChange={(e) => setTitle(e.target.value)} />
                </Box>
                <Box sx={{ mb: 2 }}>
                    <TextField fullWidth disabled={loading} label="Slug" variant="outlined" value={slug} onChange={(e) => setSlug(e.target.value)} />
                </Box>

                <CKEditor
                    editor={ClassicEditor}
                    data={text}
                    onChange={(event, editor) => {
                        const data = editor.getData();
                        setText(data);
                    }}
                />

                <Box sx={{ mb: 2 }}>{text}</Box>

                <Button disabled={loading} onClick={() => navigate("/articles")}>
                    Болих
                </Button>
                <Button disabled={loading} variant="contained" onClick={submit}>
                    Хадгалах
                </Button>
            </Container>
        </>
    );
}

function CategorySelector({ value, onChange }) {
    const { data: list } = useSWR("categories", adminFetcher);
    if (!list) return null;

    return (
        <FormControl fullWidth sx={{ mb: 2 }}>
            <InputLabel id="demo-simple-select-label">Ангилал</InputLabel>
            <Select labelId="demo-simple-select-label" id="demo-simple-select" value={value} label="Ангилал" onChange={(e) => onChange(e.target.value)}>
                {list.map((item) => (
                    <MenuItem key={item._id} value={item._id}>
                        {item.name}
                    </MenuItem>
                ))}
            </Select>
        </FormControl>
    );
}
