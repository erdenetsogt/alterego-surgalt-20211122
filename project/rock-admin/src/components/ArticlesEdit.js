import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import { Button, DialogTitle, Dialog, TextField, DialogContent, DialogActions } from "@mui/material";
import { toast } from "react-toastify";
import adminFetcher from "../utils/adminFetcher";

export default function CategoriesEdit({ onChange, editing, onClose }) {
    const [name, setName] = useState("");
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if (editing) {
            adminFetcher(`categories/${editing}`).then((data) => {
                setName(data.name);
            });
        }
    }, [editing]);

    async function submit() {
        setLoading(true);

        const form = { name };

        await adminFetcher(`categories/${editing}`, "PUT", form);

        setLoading(false);
        onClose();
        onChange();
        setName("");
        toast("Амжилттай заслаа");
    }

    return (
        <Dialog onClose={() => !loading && onClose()} open={!!editing}>
            <DialogTitle>Засах</DialogTitle>
            <DialogContent>
                <Box sx={{ pt: 2 }}>
                    <TextField disabled={loading} label="Нэр" variant="outlined" value={name} onChange={(e) => setName(e.target.value)} />
                </Box>
            </DialogContent>
            <DialogActions>
                <Button disabled={loading} onClick={onClose}>
                    Болих
                </Button>
                <Button disabled={loading} variant="contained" onClick={submit}>
                    Хадгалах
                </Button>
            </DialogActions>
        </Dialog>
    );
}
