import React, { useEffect, useState } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { Button, Typography } from "@mui/material";
import ConfirmDelete from "./ConfirmDelete";
import CategoriesEdit from "./CategoriesEdit";
import { toast } from "react-toastify";
import CircularProgress from "@mui/material/CircularProgress";
import Box from "@mui/material/Box";
import adminFetcher from "../utils/adminFetcher";
import { gql, useQuery } from "@apollo/client";

const GET_ARTICLES = gql`
    query GetArticles {
        articles {
            _id
            title
            slug
        }
    }
`;

export default function ArticlesList({ changedAt }) {
    const { loading, error, data, refetch } = useQuery(GET_ARTICLES);

    const [deleting, setDeleting] = useState("");
    const [editing, setEditing] = useState("");

    useEffect(() => {
        refetch();
        // eslint-disable-next-line
    }, [changedAt]);

    async function handleDelete() {
        // setLoading(true);
        await adminFetcher(`categories/${deleting}`, "DELETE");

        // setLoading(false);
        setDeleting("");
        // loadList();
        toast("Амжилттай устгалаа");
    }

    if (loading) {
        return (
            <Box sx={{ textAlign: "center" }}>
                <CircularProgress />
            </Box>
        );
    }

    if (error) return <p>Error :(</p>;

    const { articles } = data;

    if (articles.length === 0) {
        return (
            <Box sx={{ textAlign: "center" }}>
                <Typography variant="h1" sx={{ opacity: 0.3 }}>
                    Хоосон
                </Typography>
            </Box>
        );
    }

    return (
        <>
            <TableContainer component={Paper}>
                <Table aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell></TableCell>
                            <TableCell>Нэр</TableCell>
                            <TableCell></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {articles.map((row, index) => (
                            <TableRow key={row._id}>
                                <TableCell>{index + 1}</TableCell>
                                <TableCell>
                                    {row.title} ({row.slug})
                                </TableCell>
                                <TableCell align="right">
                                    <Button color="error" onClick={() => setDeleting(row._id)}>
                                        Устгах
                                    </Button>
                                    <Button onClick={() => setEditing(row._id)}>Засах</Button>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>

            <ConfirmDelete loading={loading} open={deleting} onClose={() => setDeleting("")} onCorfirm={handleDelete} />
            <CategoriesEdit editing={editing} onClose={() => setEditing("")} onChange={() => {}} />
        </>
    );
}
