export default function adminFetcher(path, method = "GET", form) {
    return fetch(`http://localhost:8080/admin/${path}`, {
        method: method,
        headers: {
            "Content-Type": "application/json",
            token: localStorage.getItem("token"),
        },
        body: form ? JSON.stringify(form) : undefined,
    })
        .then((response) => {
            if (response.status === 200) {
                return response.json();
            } else if (response.status === 204) {
                return null;
            }
        })
        .then((data) => {
            return data;
        });
}
