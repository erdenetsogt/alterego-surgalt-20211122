import CssBaseline from "@mui/material/CssBaseline";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Header from "./components/Header";
import Categories from "./components/Categories";
import Articles from "./components/Articles";
import ArticlesNew from "./components/ArticlesNew";
import Login from "./components/Login";
import Users from "./components/Users";
import { pink, teal } from "@mui/material/colors";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useEffect, useState } from "react";
import adminFetcher from "./utils/adminFetcher";
import { ApolloClient, InMemoryCache, ApolloProvider } from "@apollo/client";

const client = new ApolloClient({
    uri: "http://localhost:8080/graphql",
    cache: new InMemoryCache(),
});

const theme = createTheme({
    palette: {
        primary: {
            main: teal[500],
        },
        secondary: {
            main: pink[500],
        },
    },
});

function App() {
    const [status, setStatus] = useState("loading");

    useEffect(() => {
        adminFetcher(`users/me`).then((data) => {
            if (data) {
                setStatus("loggedIn");
            } else {
                setStatus("unAuthorized");
            }
        });
    }, []);

    if (status === "loading") return null;

    return (
        <ApolloProvider client={client}>
            <BrowserRouter>
                <ThemeProvider theme={theme}>
                    <CssBaseline />
                    {status === "loggedIn" && <Main />}
                    {status === "unAuthorized" && <Login />}
                </ThemeProvider>
            </BrowserRouter>
        </ApolloProvider>
    );
}

function Main() {
    return (
        <>
            <Header />
            <ToastContainer />

            <Routes>
                <Route path="/" element={<div>Home</div>} />
                <Route path="users" element={<Users />} />
                <Route path="categories" element={<Categories />} />
                <Route path="articles" element={<Articles />} />
                <Route path="articles/new" element={<ArticlesNew />} />
            </Routes>
        </>
    );
}

export default App;
