var express = require("express");
var router = express.Router();
const Article = require("../../models/Article");

router.get("/:slug", async (req, res) => {
    const { slug } = req.params;

    const one = await Article.findOne({ slug });
    res.json(one);
});

module.exports = router;
