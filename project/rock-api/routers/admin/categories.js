var express = require("express");
var router = express.Router();
const Category = require("../../models/Category");

// get list
router.get("/", async (req, res) => {
    const list = await Category.find();
    res.json(list);
});

// get one
router.get("/:id", async (req, res) => {
    const { id } = req.params;
    const one = await Category.findById(id);
    res.json(one);
});

// create
router.post("/", async (req, res) => {
    const form = req.body;
    const newCategory = await Category.create(form);
    res.json(newCategory);
});

// update
router.put("/:id", async (req, res) => {
    const { id } = req.params;
    const form = req.body;
    await Category.updateOne({ _id: id }, form);
    res.sendStatus(204);
});

// delete
router.delete("/:id", async (req, res) => {
    const { id } = req.params;
    await Category.deleteOne({ _id: id });
    res.sendStatus(204);
});

module.exports = router;
