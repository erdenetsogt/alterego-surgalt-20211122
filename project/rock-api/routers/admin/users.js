var express = require("express");
var router = express.Router();
const User = require("../../models/User");
var bcrypt = require("bcryptjs");

router.get("/me", async (req, res) => {
    const me = await User.findById(req.userId);
    res.json(me);
});

// get list
router.get("/", async (req, res) => {
    const list = await User.find();
    res.json(list);
});

// get one
router.get("/:id", async (req, res) => {
    const { id } = req.params;
    const one = await User.findById(id);
    res.json(one);
});

// create
router.post("/", async (req, res) => {
    const { username, password } = req.body;

    var salt = bcrypt.genSaltSync(10);
    var hash = bcrypt.hashSync(password, salt);

    const newUser = await User.create({ username, password: hash });
    res.json(newUser);
});

// update
router.put("/:id", async (req, res) => {
    const { id } = req.params;
    const { username, password } = req.body;

    const form = { username };

    if (password) {
        var salt = bcrypt.genSaltSync(10);
        var hash = bcrypt.hashSync(password, salt);
        form.password = hash;
    }

    await User.updateOne({ _id: id }, form);
    res.sendStatus(204);
});

// delete
router.delete("/:id", async (req, res) => {
    const { id } = req.params;
    await User.deleteOne({ _id: id });
    res.sendStatus(204);
});

module.exports = router;
