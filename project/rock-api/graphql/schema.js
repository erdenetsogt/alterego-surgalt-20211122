const { gql } = require("apollo-server-express");

const typeDefs = gql`
    type Query {
        articles: [Article]
        publishedArticles(categoryId: String): [Article]
        oneArticleById(id: ID): Article
        oneArticleBySlug(slug: String): Article
    }

    type Mutation {
        addArticle(title: String!, slug: String, categoryId: String, text: String): Article
        updateArticle(id: ID!, title: String, slug: String): Boolean
        deleteArticle(id: ID!): Boolean
    }

    type Article {
        _id: ID
        categoryId: String
        title: String
        slug: String
        text: String
        viewCount: Int
        published: Boolean
    }
`;

module.exports = typeDefs;
