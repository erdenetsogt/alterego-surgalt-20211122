const Article = require("../models/Article");

const resolvers = {
    Query: {
        articles: async () => {
            const list = await Article.find();
            return list;
        },
        publishedArticles: () => {
            return null;
        },
        oneArticleById: async () => {
            return null;
        },
        oneArticleBySlug: () => {
            return null;
        },
    },

    Mutation: {
        addArticle: async (_, args) => {
            const { slug } = args;
            const duplicate = await Article.findOne({ slug });
            if (duplicate) {
                return null;
            }
            const newArticle = await Article.create(args);
            return newArticle;
        },
        updateArticle: async () => {},
        deleteArticle: async () => {},
    },
};

module.exports = resolvers;
