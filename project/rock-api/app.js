const express = require("express");
const app = express();
const port = 8080;
const cors = require("cors");
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");
const mongoose = require("mongoose");
const { JWT_SECRET } = require("./secrets");
const User = require("./models/User");

mongoose.connect("mongodb+srv://rockAdmin:NuzXLnuPtrY67rS8@cluster0.tprtt.mongodb.net/rock?retryWrites=true&w=majority");

app.use(cors());

app.use(express.json());

function isAdmin(req, res, next) {
    const token = req.header("token");
    if (!token) return res.sendStatus(401);

    try {
        var decoded = jwt.verify(token, JWT_SECRET);
        req.userId = decoded.userId;
        next();
    } catch (err) {
        return res.sendStatus(401);
    }
}

app.use("/admin/categories", isAdmin, require("./routers/admin/categories"));
app.use("/admin/users", isAdmin, require("./routers/admin/users"));

app.use("/admin/login", async (req, res) => {
    const { username, password } = req.body;

    const user = await User.findOne({ username });

    if (!user) return res.sendStatus(401);

    if (!bcrypt.compareSync(password, user.password)) return res.sendStatus(401);

    var token = jwt.sign({ userId: user._id }, JWT_SECRET);

    res.json([token]);
});

app.use("/client/categories", require("./routers/client/categories"));
app.use("/client/articles", require("./routers/client/articles"));

const { ApolloServer } = require("apollo-server-express");

async function startApolloServer(typeDefs, resolvers) {
    const server = new ApolloServer({ typeDefs, resolvers });
    await server.start();
    server.applyMiddleware({ app });
    app.listen({ port }, () => console.log(`Server ready at http://localhost:${port}${server.graphqlPath}`));
}

const typeDefs = require("./graphql/schema");
const resolvers = require("./graphql/resolvers");
startApolloServer(typeDefs, resolvers);
