const mongoose = require("mongoose");

const Category = mongoose.model(
    "Category",
    {
        name: { type: String, required: true },
        color: { type: String },
    },
    "categories"
);

module.exports = Category;
