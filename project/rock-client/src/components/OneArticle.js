/** @jsxImportSource @emotion/react */
import { useEffect, useState } from "react";
import { FacebookProvider, Comments } from "react-facebook";
import { useParams } from "react-router-dom";
import clientFetcher from "../utils/clientFetcher";

export default function OneArticles() {
    const { slug } = useParams();
    const [article, setArticle] = useState({});

    async function loadArticleBySlug(slug) {
        const data = await clientFetcher(`articles/${slug}`);
        setArticle(data);
    }

    useEffect(() => {
        loadArticleBySlug(slug);
    }, [slug]);

    function createMarkup() {
        return { __html: article.text };
    }

    return (
        <div className="container" style={{ maxWidth: 600 }}>
            <h1 className="title">{article.title}</h1>

            <div className="is-flex block is-justify-content-space-between">
                <span className="tag is-danger">Улс төр</span>
                <div className="has-text-grey">
                    <small>20 минутын өмнө</small>
                </div>
            </div>

            <div className="content" dangerouslySetInnerHTML={createMarkup()} />

            <FacebookProvider appId="123456789">
                <Comments href="http://time.mn/rad.html" />
            </FacebookProvider>
        </div>
    );
}
