import { useEffect, useState } from "react";
import clientFetcher from "../utils/clientFetcher";

// const data = [
//     { id: 1, name: "Улс төр", color: "is-info" },
//     { id: 2, name: "Эдийн засаг", color: "is-success" },
//     { id: 3, name: "Спорт", color: "is-warning" },
//     { id: 3, name: "Дэлхий", color: "is-link" },
//     { id: 3, name: "Шар мэдээ", color: "is-primary" },
//     { id: 3, name: "Шар мэдээ", color: "is-danger" },
// ];

export default function Categories() {
    const [list, setList] = useState([]);

    async function loadCategories() {
        const data = await clientFetcher("categories");
        setList(data);
    }

    useEffect(() => {
        loadCategories();
    }, []);

    return (
        <div className="buttons">
            {list.map((category) => (
                <button key={category._id} className={`button ${category.color}`} style={{ flexGrow: 1 }}>
                    {category.name}
                </button>
            ))}
        </div>
    );
}
